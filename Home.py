import streamlit as st
import os
import requests
from datetime import datetime

def getAPOD():
    apiKey = os.getenv("NASAAPI")
    today = datetime.today()
    today = today.strftime("%Y-%m-%d")
    response = requests.get("https://api.nasa.gov/planetary/apod?"
                            f"api_key={apiKey}&date={today}")
    data = response.json()
    return data


data = getAPOD()

st.header(data['title'])
st.image(data['url'])
st.write(data['explanation'])


